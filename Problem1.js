// Problem1:
// Create two functions, racePromise1 and racePromise2.
// Both functions should return a Promise that resolves with a unique success message after a random delay between 1 and 3 seconds.
// Implement a third function, racePromises, that races the execution of the two functions. The Promise should resolve with the message of the function that resolves first.


const racePromise1=new Promise((resolve,reject)=>{

        setTimeout(()=>{
            resolve("racePromise1 is resolved");
        },1000)
})

const racePromise2= new Promise((resolve,reject)=>{

        setTimeout(()=>{
            resolve("racePromise2 is resolved");
        },3000)

    
})

const race=Promise.race([racePromise1,racePromise2]);

race.then((res)=>{
    console.log(res);
})