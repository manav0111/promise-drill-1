// Problem2:
// Create a function named composePromises.
// This function should take an array of Promises as input and return a new Promise.
// The new Promise should resolve with an array containing the results of all the input Promises, in the order they were provided.

const promise1 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve("Promise 1 resolved");
  }, 1000);
});

const promise2 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve("Promise 2 is resolved");
  }, 3000);
});

const promise3 = new Promise((resolve, reject) => {
  setTimeout(() => {
    reject("Promise 3 is rejected");
  }, 5000);
});

const composePromises = () => {
  const resultPromise = Promise.allSettled([promise1, promise2, promise3]);

  return resultPromise;
};
composePromises()
  .then((resultArray) => {
    resultArray.forEach((obj) => {
      if (obj.status == "fulfilled")
        console.log(
          `The Status is: ${obj.status} and the value is: ${obj.value}`
        );
      else
        console.log(
          `The Status is: ${obj.status} and the Reason is: ${obj.reason}`
        );
    });
  })
  .catch((err) => {
    console.log(err);
  });
