// Problem3:
// Create a function named dynamicChain.
// This function should take an array of functions that return Promises as input.
// Use a loop to dynamically chain the Promises returned by each function in the array.
// The final Promise should resolve with the result of the last function in the array.

const promise1 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve("Promise 1 resolved");
  }, 1000);
});

const promise2 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve("Promise 2 is resolved");
  }, 3000);
});

const promise3 = new Promise((resolve, reject) => {
  setTimeout(() => {
    reject("Promise 3 is resolved");
  }, 5000);
});

const dynamicChain = (arr) => {
  let lastpromise = Promise.resolve();
  arr.forEach((promise) => {
    lastpromise = lastpromise
      .then(() => {
        return promise;
      })
      .catch(() => {
        return promise;
      });
  });

  return lastpromise;
};

const arr = [promise1, promise2, promise3];
dynamicChain(arr)
  .then((result) => {
    console.log(result);
  })
  .catch((err) => {
    console.log(err);
  });
