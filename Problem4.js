const promise1 = new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve("Promise 1 resolved");
    }, 1000);
  }).catch((err)=>console.log(err));
  
const promise2 = new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve("Promise 2 is resolved");
    }, 3000);
  }).catch((err)=>console.log(err));
  
const promise3 = new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve("Promise 3 is resolved");
    }, 5000);
  }).catch((err)=>console.log(err));

const parallelLimit = (arr, limit) => {
    let resultArr = [];
    let initialIndex = 0;
    const length = arr.length;

    return new Promise((resolve, reject) => {
        const helper = () => {
            const sliceArr = arr.slice(initialIndex, initialIndex + limit);

            Promise.allSettled(sliceArr)
                .then((results) => {
                    resultArr = resultArr.concat(results);
                    initialIndex += limit;

                    if (initialIndex < length) {
                        helper();
                    } else {
                        resolve(resultArr);
                    }
                })
                .catch((err) => {
                    reject(err);
                });
        };

        helper();
    });
};

let arr = [promise1, promise2, promise3];
const limit = 2;

parallelLimit(arr, limit)
    .then((res) => {
        console.log(res);
    })
    .catch((err) => console.log(err));
